setopt interactive_comments
setopt autocd
autoload -U colors && colors
PS1="%B%{$fg[red]%} %~%{$fg[yellow]%} >%{$reset_color%}%b "
stty stop undef

HISTSIZE=123456789
SAVEHIST=123456789
HISTFILE="${XDG_CACHE_HOME:-$HOME/.cache}/history"

autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)

bindkey -v
export KEYTIMEOUT=1

bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

function zle-keymap-select () {
    case $KEYMAP in
        vicmd) echo -ne '\e[1 q';;
        viins|main) echo -ne '\e[5 q';;
    esac
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q'
preexec() { echo -ne '\e[5 q' ;}

autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line
bindkey -M vicmd '^[[P' vi-delete-char
bindkey -M vicmd '^e' edit-command-line
bindkey -M visual '^[[P' vi-delete

[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/zsh/alias.sh" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/zsh/alias.sh"
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/zsh/vars.sh" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/zsh/vars.sh"
[ -f "/usr/share/zsh/site-functions/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh" ] && \
source "/usr/share/zsh/site-functions/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh"
