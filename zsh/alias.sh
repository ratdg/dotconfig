alias sudo='doas'
alias ls='ls -A --color=auto --group-directories-first'
alias grep='grep --color=auto'
alias wget='wget --no-cache --no-cookies --secure-protocol=TLSv1_3 --https-only'
