set nocompatible
set encoding=utf-8
set history=1000
set colorcolumn=80
set cursorcolumn
set noemoji

" tab if needed
set autoindent
set smartindent
set cindent

" line wrap and perserve horiz blocks of code
set breakindent

" re-read file when changed
set autoread

" backspace over exery special character
set backspace=indent,eol,nostop

" disable backups
set nobackup

" rename file instead of copying and overriting
set backupcopy=no

" line going from cursor
set cursorline

" don't enable folding by default
set nofoldenable

" folding options
set foldmethod=indent
set foldnestmax=3

" highlight search queries
set hlsearch

" ignore caps in search queries
set ignorecase

" incremental search
set incsearch

" show line count
set number

" show line number relative to cursor
set relativenumber

" don't redraw document when not needed (increase performance)
set lazyredraw

" set amount of spaces per tab/shift"
set shiftwidth=3
set tabstop=3

" set color-scheme from .vim/colors"
colorscheme badwolf

set secure
